﻿using Unity.FPS.Game;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace Unity.FPS.UI
{
    public class LoadCurrentSceneButton : MonoBehaviour
    {

        void Update()
        {
            if (EventSystem.current.currentSelectedGameObject == gameObject
                && Input.GetButtonDown(GameConstants.k_ButtonNameSubmit))

            {
                LoadTargetScene();
            }
        }

        public void LoadTargetScene()
        {
            Debug.Log(PlayerPrefs.GetString("lastSceneName"));
            SceneManager.LoadScene(PlayerPrefs.GetString("lastSceneName"));
        }
    }
}